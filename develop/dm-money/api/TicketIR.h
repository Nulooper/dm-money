/////////////////////////////////////////////////////////////
/*Filename   : TicketIR.h                                        */
/*Create Date: 2014-09-05                                  */
/*Author     : Wang Zhiwei 王志伟                                */
/////////////////////////////////////////////////////////////
#ifndef _TICKET_IR_H_
#define _TICKET_IR_H_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

const static std::string DEFAULT_NULL_TICKET = "0";
const static std::string DEFAULT_UNREG_TICKET = "0";
const static std::string DEFAULT_FREE_TICKET = "0";

const static std::string NULL_TICKET_TYPE = "NULL_TICKET_RULE";
const static std::string UNREG_TICKET_TYPE = "UNREG_TICKET_RULE";
const static std::string FREE_TICKET_TYPE = "FREE_TICKET_RULE";

const static std::string DAY_TICKET_TYPE = "DAY_TICKET_RULE";
const static std::string WEEK_TICKET_TYPE = "WEEK_TICKET_RULE";
const static std::string MONTH_TICKET_TYPE = "MONTH_TICKET_RULE";
const static std::string TICKET_TYPE = "TICKET_RULE";

const static std::string ID_RULE_DELIM = "\t";
const static std::string OPEN_CLOSE_DELIM = "|";
const static std::string OPEN_RULE_DELIM = "--";
const static std::string RULE_TYPE_DELIM = "&";

const int mon_day[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};//每月的天数.

class TicketIR
{
public:
	static bool search(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);

public:
	///////
	//处理规则黑名单
	static bool checkCloseDayTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);
	static bool checkCloseWeekTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);

	///////
	//处理规则白名单.
	//DAY_TICKET_RULE
    static bool checkOpenDayTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);
    //WEEK_TICKET_RULE
    static bool checkOpenWeekTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);
    //MONTH_TICKET_RULE
    static bool checkOpenMonthTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);
    //TICKET_RULE
    static bool checkOpenTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket);

public:
	static void vector2String(const std::vector<std::string>& time_pair_vec, const std::string& delim, std::string& out_str);
    static void split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec);
    static std::string int2String(int in_number);
    static int string2int(const std::string& in_str);
    static std::string getStrBetween(std::string in_str, std::string left, bool is_left_forward, std::string right, bool is_right_forward);
    static bool isInInterval(const std::string& in_number_str, const std::string& interval_left_str, const std::string& interval_right_str);

    static int getWDay(const time_t& t, int zone = 8);
    static time_t toTime(const std::string& s, int zone = 8);
    static int compareDayStr(const std::string& a,const std::string& b);
    static std::string getDayOfWeek(const std::string& in_date);
};

bool TicketIR::search(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	std::vector<std::string> rule_items;
	split(in_rule, OPEN_CLOSE_DELIM, rule_items);
	if(rule_items.size() != 3)
	{
		// std::cout << "size[" << rule_items.size() << "]" << endl;
		std::cout << "输入规则不合法! rule[" << in_rule << "] in_date[" << in_date << "]" << std::endl;
		out_ticket = DEFAULT_UNREG_TICKET;
		return false;
	}

	bool isSure = false;
	if (rule_items[2] == "SURE")
	{
		isSure = true;
	}

	//step1. 首先检索close_rules, 如果命中，直接返回结果.
	//NOTE: close_rules只有两种类型:NO, DAY_TICKET_RULE, WEEK_TICKET_RULE
	std::vector<std::string> close_items;
	split(rule_items[1], RULE_TYPE_DELIM, close_items);
	std::string close_rule = close_items[0];
	std::string close_type = close_items[1];
	std::string normalized_date = in_date.substr(4);

	if(DAY_TICKET_TYPE == close_type)
	{
		if (checkCloseDayTicketRule(close_rule, in_date, out_ticket))
		{
			std::cout << "命中黑名单:DAY_TICKET_TYPE[" + close_rule + "] in_date[" << in_date << "]" << std::endl;
			return isSure;
		}
	}

	if (WEEK_TICKET_TYPE == close_type)
	{
		if (checkCloseWeekTicketRule(close_rule, in_date, out_ticket))
		{
			std::cout << "命中黑名单:WEEK_TICKET_TYPE[" << close_rule << "] in_date[" << in_date << "]" << std::endl;
			return isSure;
		}
	}
	
	//step2. 检索open_rules. 不用考虑close_rules(step1已经解决).
	std::vector<std::string> open_rules;
	split(rule_items[0], OPEN_RULE_DELIM, open_rules);
	// std::cout << "open_rules size[" << open_rules.size() << "]" << endl;
	std::map<std::string, std::string> opentype_rule_map;
	for(std::vector<std::string>::iterator iter = open_rules.begin(); iter != open_rules.end(); ++iter)
	{
		std::vector<std::string> tmp_vec;
		split(*iter, RULE_TYPE_DELIM, tmp_vec);
		opentype_rule_map[tmp_vec[1]] = tmp_vec[0];
		// std::cout << tmp_vec[1] << "," << tmp_vec[0] << std::endl;
	}

	// std::cout << "------" << opentype_rule_map[TYPE_ONLY_TIME] << endl;

	//根据优先级依次进行时间规则检索.
	std::map<std::string, std::string>::const_iterator end_iter = opentype_rule_map.end();
	if (opentype_rule_map.find(NULL_TICKET_TYPE) != end_iter)
	{
		out_ticket = DEFAULT_NULL_TICKET;
		std::cout << "命中 NULL_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
		return isSure;
	}
	if (opentype_rule_map.find(UNREG_TICKET_TYPE) != end_iter)
	{
		out_ticket = DEFAULT_UNREG_TICKET;
		std::cout << "命中 UNREG_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
		return isSure;
	}
	if (opentype_rule_map.find(FREE_TICKET_TYPE) != end_iter)
	{
		out_ticket = DEFAULT_FREE_TICKET;
		std::cout << "命中 FREE_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
		return isSure;
	}
	if (opentype_rule_map.find(DAY_TICKET_TYPE) != end_iter)
	{
		if (checkOpenDayTicketRule(opentype_rule_map[DAY_TICKET_TYPE], in_date, out_ticket))
		{
			std::cout << "命中 DAY_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
			return isSure;	
		}
		
	}
	if (opentype_rule_map.find(WEEK_TICKET_TYPE) != end_iter)
	{
		if (checkOpenWeekTicketRule(opentype_rule_map[WEEK_TICKET_TYPE], in_date, out_ticket))
		{
			std::cout << "命中 WEEK_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
			return isSure;	
		}
	}
	if (opentype_rule_map.find(MONTH_TICKET_TYPE) != end_iter)
	{
		if (checkOpenMonthTicketRule(opentype_rule_map[MONTH_TICKET_TYPE], in_date, out_ticket))
		{
			std::cout << "命中 MONTH_TICKET_TYPE in_date[" << in_date << "]" << std::endl;
			return isSure;	
		}
	}
	if (opentype_rule_map.find(TICKET_TYPE) != end_iter)
	{
		// std::cout << "checking TIME_ONLY" << endl;
		if (checkOpenTicketRule(opentype_rule_map[TICKET_TYPE], in_date, out_ticket))
		{
			std::cout << "命中 TICKET_TYPE in_date[" << in_date << "]" << std::endl;
			return isSure;	
		}
	}
	// std::cout << "Match Nothing!";
	out_ticket = DEFAULT_UNREG_TICKET;
	std::cout << "没有任何命中! in_date[" << in_date << "]" << std::endl;
	return isSure;
}

//finish.
bool TicketIR::checkCloseDayTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	return checkOpenDayTicketRule(in_rule, in_date, out_ticket);
}

//finish.
bool TicketIR::checkCloseWeekTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	return checkOpenWeekTicketRule(in_rule, in_date, out_ticket);
}

//finish.
bool TicketIR::checkOpenDayTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	std::string normalized_date = in_date.substr(4);

	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string left_date, right_date;
	for(std::vector<std::string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{
		left_date = getStrBetween(*iter, "{", true, "-", true);
		right_date = getStrBetween(*iter, "-", true, ":", true);
		if ((compareDayStr(left_date, normalized_date) <= 0) && (compareDayStr(normalized_date, right_date) <= 0))
		{
			out_ticket = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}
	return false;
}

//finsih.
bool TicketIR::checkOpenWeekTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	std::string in_weekday = getDayOfWeek(in_date);
	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string weekday_left, weekday_right;
	for (std::vector<std::string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{
		weekday_left = getStrBetween(*iter, "{", true, "-", true);
		weekday_right = getStrBetween(*iter, "-", true, ":", true);
		if(isInInterval(in_weekday, weekday_left, weekday_right))
		{
			out_ticket = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}

	return false;
}

//finish.
bool TicketIR::checkOpenMonthTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	std::string normalized_date = in_date.substr(4);
	std::string in_month = normalized_date.substr(0, normalized_date.size() / 2);
	std::vector<std::string> vec;
	split(in_rule, ",", vec);
	std::string month_left, month_right;
	for(std::vector<std::string>::iterator iter = vec.begin(); iter != vec.end(); ++iter)
	{	
		month_left = getStrBetween(*iter, "{", true, "-", true);
		month_right = getStrBetween(*iter, "-", true, ":", true);
		// std::cout << "month_left[" << month_left << "]month_right[" << month_right << "]in_month[" << in_month << "]in_date[" << in_date <<"]" << endl;
		if (isInInterval(in_month, month_left, month_right))
		{
			out_ticket = getStrBetween(*iter, ":", true, "}", false);
			return true;
		}
	}

	return false;
}

//finish.
bool TicketIR::checkOpenTicketRule(const std::string& in_rule, const std::string& in_date, std::string& out_ticket)
{
	out_ticket = getStrBetween(in_rule, "{", true, "}", false);
	return true;
}

void TicketIR::split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
    std::string::size_type pos = 0;
    std::string::size_type len = in_str.length();
    std::string::size_type delim_len = delim.length();
    if (0 == len || 0 == delim_len)
    {
    	return;
    }
    while (pos < len)
    {
        std::string::size_type find_pos = in_str.find(delim, pos);
        if (find_pos == -1)
        {
            out_vec.push_back(in_str.substr(pos, len - pos));
            break;
        }
        out_vec.push_back(in_str.substr(pos, find_pos - pos));
        pos = find_pos + delim_len;
    }
}

std::string TicketIR::int2String(int in_number)
{
    std::stringstream ss;
	ss << in_number;
    std::string number_str;
	ss >> number_str;
    
	return number_str;
}

int TicketIR::string2int(const std::string& in_str)
{
    std::stringstream ss;
	ss << in_str;
	int number;
	ss >> number;
    
	return number;
}

std::string TicketIR::getStrBetween(std::string in_str, std::string left, bool is_left_forward, std::string right, bool is_right_forward)
{
	std::size_t left_index;
	if (is_left_forward)
	{
		left_index = in_str.find(left);
	}
	else
	{
		left_index = in_str.rfind(left);
	}

	std::size_t right_index;
	if (is_right_forward)
	{
		right_index = in_str.find(right);
	}
	else
	{
		right_index = in_str.rfind(right);
	}

	return in_str.substr(left_index + left.length(), right_index - left_index - right.length());
}

bool TicketIR::isInInterval(const std::string& in_number_str, const std::string& interval_left_str, const std::string& interval_right_str)
{
	int in_number = string2int(in_number_str);
	int left = string2int(interval_left_str);
	int right = string2int(interval_right_str);

	return left <= in_number && in_number <= right;
}

time_t TicketIR::toTime(const std::string& s, int zone)
{
	const char* s_c = s.c_str();
	char* pstr;
	long year, month, day, hour, min, sec;
	year = strtol(s_c, &pstr, 10);
	month = (year / 100) % 100;
	day = (year % 100);
	year = year / 10000;
	hour = min = sec = 0;
	if(*pstr)
	{
		hour = strtol(++pstr, &pstr, 10);
		if(*pstr)
		{
			min = strtol(++pstr, &pstr, 10);
			if (*pstr)
			{
				sec = strtol(++pstr, &pstr, 10);
			}
		}
	}
	//printf("%d %d %d\n", hour,min,sec);

    int leap_year = (year - 1969) / 4;	//year-1-1968
    int i = (year - 2001) / 100;		//year-1-2000
    leap_year -= ((i / 4) * 3 + i % 4);
    int day_offset = 0;
    for (i = 0; i < month - 1; i++)
    	day_offset += mon_day[i];
    bool isLeap = ((year % 4 == 0 && year % 100 != 0)||(year % 400 == 0));
    if (isLeap && month>2)
    	day_offset += 1;
    day_offset += (day-1);
    day_offset += ((year - 1970) * 365 + leap_year);
    int hour_offset = hour - zone;
    time_t ret = day_offset * 86400 + hour_offset * 3600 + min * 60 + sec;

    return ret;
}


int TicketIR::compareDayStr(const std::string& a, const std::string& b)
{
	if (a == b)
		return 0;

	time_t a_t = toTime("2014" + a + "_00:00", 0);
	time_t b_t = toTime("2014" + b + "_00:00", 0);

	return (a_t - b_t) / 86400;
}

int TicketIR::getWDay(const time_t& t, int zone)
{
	struct tm TM;
	time_t nt = t + zone * 3600;
	gmtime_r(&nt, &TM);
	int ret = (TM.tm_wday + 6) % 7 + 1;
	return ret;
}

std::string TicketIR::getDayOfWeek(const std::string& in_date)
{
	
	time_t tt = TicketIR::toTime(in_date+"_00:00");
    int wd = TicketIR::getWDay(tt);

	return int2String(wd);
}

#endif