/////////////////////////////////////////////////////////////
/*Filename   : TicketIR.h                                        */
/*Create Date: 2014-07-011                                  */
/*Author     : Wang Zhiwei 王志伟                                */
/////////////////////////////////////////////////////////////
#ifndef _TICKET_IR_H_
#define _TICKET_IR_H_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

const static std::string RETURN_UNRECOG = "0";
const static std::string RETURN_UNKNOWN = "0";
const static std::string RETURN_FREE = "0";

const static std::string RETURN_CLOSE = "0";

const static std::string RULE_DELIM = "|";

const static std::string ONLY_TYPE = "ONE";
const static std::string TYPE_UNKNOWN = "UKNOWN";
const static std::string TYPE_UNRECOG = "UNRECOG";

class TicketIR
{
public:
    static bool search(const std::string& rule, const std::string& in_date, std::string& out_tickets);
    
public:
    
    static void vector2String(const std::vector<std::string>& time_pair_vec, const std::string& delim, std::string& out_str);
    static void split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec);
    static std::string int2String(int in_number);
    static int string2int(const std::string& in_str);
};

bool TicketIR::search(const std::string& rule, const std::string& in_date, std::string& out_tickets)
{
	std::vector<std::string> rule_items;
    split(rule, RULE_DELIM, rule_items);
	if(rule_items.empty())
	{
		out_tickets = RETURN_UNKNOWN;
		return false;
	}
    
	std::string general_rule = rule_items[0];
	//std::cerr << "general_rule:[" << general_rule << "]" << std::endl;
	std::vector<std::string> open_items;
    split(general_rule, "&", open_items);
	std::string general_text = open_items[0];
	std::string general_type = open_items[1];
	//std::cerr << "general_text:[" << general_text << "] general_type:[" << general_type << "]" << std::endl;
    
    std::string special_rule = rule_items[1];
   // std::cerr << "special_rule:[" << special_rule << "]" << std::endl;
    std::vector<std::string> close_items;
    split(special_rule, "&", close_items);
    std::string special_text = close_items[0];
    std::string special_type = close_items[1];
    //std::cerr << "special_text:[" << special_text << "] special_type:[" << special_type << "]" << std::endl;
    
    //std::cerr << "general_text[" << general_text << "] general_type[" << general_type << "] close_text["
    //<< close_text << "] special_type["<< special_type << "]" << std::endl;
    
    if(TYPE_UNKNOWN == general_type && TYPE_UNKNOWN == special_type)
    {
    	out_tickets = RETURN_UNKNOWN;
    	std::cerr << "match TYPE_UNKNOWN" << std::endl;
    	return true;
    }
    else if(TYPE_UNRECOG == general_type && TYPE_UNRECOG == special_type)
    {
    	out_tickets = RETURN_UNRECOG;
    	std::cerr << "match TYPE_UNRECOG" << std::endl;
    	return true;
    }
    else if(ONLY_TYPE == general_type)
    {
        
    	out_tickets = general_text;
    	std::cerr << "macth ONLY_TYPE! out_tickets:[" << out_tickets << "]" << std::endl;
    	return true;
    }
    else
    {
    	out_tickets = RETURN_UNKNOWN;
    	return false;
    }
    
}

void TicketIR::vector2String(const std::vector<std::string>& time_pair_vec, const std::string& split, std::string& out_str)
{
	std::vector<std::string>::size_type size = time_pair_vec.size();
    
    if(0 == size)
    {
    	out_str = "";
    	return;
    }
    
    if(1 == size){
        out_str = time_pair_vec[0];
        return;
    }
    
    out_str = "";
    for(std::vector<std::string>::size_type i = 0; i < size - 1; ++i)
    {
        out_str += time_pair_vec[i] + split;
    }
    
    out_str += time_pair_vec[size - 1];
}

void TicketIR::split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
    std::string::size_type pos = 0;
    std::string::size_type len = in_str.length();
    std::string::size_type delim_len = delim.length();
    if (0 == len || 0 == delim_len)
    {
    	return;
    }
    while (pos < len)
    {
        std::string::size_type find_pos = in_str.find(delim, pos);
        if (find_pos == -1)
        {
            out_vec.push_back(in_str.substr(pos, len - pos));
            break;
        }
        out_vec.push_back(in_str.substr(pos, find_pos - pos));
        pos = find_pos + delim_len;
    }
}

std::string TicketIR::int2String(int in_number)
{
    std::stringstream ss;
	ss << in_number;
    std::string number_str;
	ss >> number_str;
    
	return number_str;
}

int TicketIR::string2int(const std::string& in_str)
{
    std::stringstream ss;
	ss << in_str;
	int number;
	ss >> number;
    
	return number;
}

#endif

