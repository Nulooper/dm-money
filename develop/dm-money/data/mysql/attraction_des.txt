===数据库 onlinedb

== 表的结构 attraction

|------
|字段|类型|空|默认
|------
|//**id**//|varchar(16)|否|
|name|varchar(128)|是|NULL
|name_en|varchar(256)|是|NULL
|grade|int(11)|是|NULL
|city_id|varchar(16)|是|NULL
|map_info|varchar(64)|是|NULL
|address|varchar(256)|是|NULL
|phone|varchar(128)|是|NULL
|website|varchar(256)|是|NULL
|open|varchar(256)|是|NULL
|close|varchar(256)|是|NULL
|ticket|varchar(256)|是|NULL
|description|text|是|NULL
|image|text|是|NULL
|rcmd_visit_time|varchar(64)|否|
== 转存表中的数据 attraction