package com.mioji.cli;

import com.mioji.parse.ParseRobot;

public class CLI {

	public static void main(String[] args) {
		if (args.length < 4) {
			System.out.println("请输入参数:dataPath, recogPath和unrecogPath!");
			System.out.println("\tdataPath: 待解析文本的路径，每一行表示一个景点的价格描述文本!");
			System.out.println("\trulePath: 存储解析后的价格规则的文件路径!");
			System.out.println("\trecogPath: 存储能够解析的原始文本的文件路径!");
			System.out.println("\tunrecogPath: 存储不能解析的原始文本的文件路径!");

			return;
		}

		String dataPath = args[0];
		String rulePath = args[1];
		String recogPath = args[2];
		String unrecogPath = args[3];

		ParseRobot.excute(dataPath, rulePath, recogPath, unrecogPath);
	}

}
