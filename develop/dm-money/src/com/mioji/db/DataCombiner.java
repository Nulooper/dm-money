package com.mioji.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

/**
 * 根据三个表进行相应地合并, 合并后的文件每行为: id\tcountry\tticket
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午6:06:08
 *
 */
public class DataCombiner {

	private static final String ATTRACTION_TABLE = "data/mysql/attraction.txt";
	private static final String CITY_TABLE = "data/mysql/city.txt";
	private static final String TICKET_TABLE = "data/mysql/ticket.txt";

	private static final String COMBINED_TICKET = "data/combine_ticket.txt";

	/*
	 * key: 城市id, value: 该城市所在的国家
	 */
	private static final Map<String, String> CITY_COUNTRY_MAP = new HashMap<String, String>();
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(CITY_TABLE));
			for (String line : lines) {
				String[] items = line.split("\\|");
				CITY_COUNTRY_MAP.put(items[1], items[8]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createCityIdCountry() throws IOException {
		List<String> lines = FileUtils.readLines(new File(CITY_TABLE));
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			String[] items = line.split("\\|");
			outputLines.add(items[1] + "\t" + items[8]);
		}
		FileUtils.writeLines(new File("data/cityid_country.txt"), outputLines);
	}

	/*
	 * key: 景点id, value: 所在城市id
	 */
	private static final Map<String, String> SPOT_CITY_MAP = new HashMap<String, String>();
	static {
		try {
			List<String> lines = FileUtils
					.readLines(new File(ATTRACTION_TABLE));
			for (String line : lines) {
				String[] items = line.split("\\|");
				SPOT_CITY_MAP.put(items[1], items[5]);
				System.out.println(items[1] + "\t" + items[5]);
			}
		} catch (IOException e) {

		}
	}

	public static void createSpotIdAndCityId() throws IOException {
		List<String> lines = FileUtils.readLines(new File(ATTRACTION_TABLE));
		List<String> twoColumnLines = new ArrayList<String>();
		for (String line : lines) {
			String[] items = line.split("\\|");
			twoColumnLines.add(items[1] + "\t" + items[5]);
		}
		FileUtils
				.writeLines(new File("data/spotid_cityid.txt"), twoColumnLines);
	}

	/*
	 * key: 景点id, value: 景点对应的票价信息
	 */
	private static final Map<String, String> SPOT_TICKET_MAP = new HashMap<String, String>();
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(TICKET_TABLE));
			for (String line : lines) {
				String[] items = line.split("\t");
				int length = items.length;
				if (1 == length) {
					SPOT_TICKET_MAP.put(items[0], "NULL");
				} else {
					SPOT_TICKET_MAP.put(items[0], items[1]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void combineTo() throws IOException {
		List<String> spotCountryTicketLines = new ArrayList<String>();
		for (String key : SPOT_TICKET_MAP.keySet()) {
			String cityId = SPOT_CITY_MAP.get(key);
			String country = CITY_COUNTRY_MAP.get(cityId);
			spotCountryTicketLines.add(key + "\t" + country + "\t"
					+ SPOT_TICKET_MAP.get(key));
		}
		FileUtils.writeLines(new File(COMBINED_TICKET), spotCountryTicketLines);
	}

	public static void createOnlyTicket() throws IOException {
		List<String> lines = FileUtils.readLines(new File(
				"data/spotid_ticket.txt"));
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			String[] items = line.split("\t");
			if (items.length == 1) {
				outputLines.add("null");
			} else {
				outputLines.add(items[1].trim().toLowerCase());
			}

		}
		Collections.sort(outputLines);
		FileUtils.writeLines(new File("data/only_ticket.txt"), outputLines);
	}

	public static void main(String[] args) throws IOException {
		// createSpotIdAndCityId();
		// createCityIdCountry();
		createOnlyTicket();
	}

}
