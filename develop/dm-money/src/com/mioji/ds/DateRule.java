package com.mioji.ds;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:57:52
 *
 */
public class DateRule extends TicketRule {
	public static final String RULE_TYPE = "DATE_RULE";

	private MiojiPair dayPair;

	public static DateRule createInstance(MiojiPair dayPair, String price,
			String unit) {
		return new DateRule(dayPair, price, unit);
	}

	public DateRule(MiojiPair dayPair, String price, String unit) {
		super(price, unit);
		this.dayPair = dayPair;
	}

	public MiojiPair getDayPair() {
		return dayPair;
	}

	@Override
	public String toString() {
		return this.dayPair.toString() + ":" + this.getPrice() + "-"
				+ this.getUnit();
	}

	public static void main(String[] args) {
	}

}
