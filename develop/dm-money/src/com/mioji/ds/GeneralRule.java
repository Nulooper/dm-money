package com.mioji.ds;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/***
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:57:42
 *
 */
public class GeneralRule {
	private List<DateRule> dateRuleList;
	private List<WeekDayRule> weekDayRuleList;
	private List<MonthRule> monthRuleList;
	private List<QuarterRule> quarterRuleList;

	public static GeneralRule createInstance(List<DateRule> dateRuleList,
			List<WeekDayRule> weekDayRuleList, List<MonthRule> monthRuleList,
			List<QuarterRule> quarterRuleList) {

		return new GeneralRule(dateRuleList, weekDayRuleList, monthRuleList,
				quarterRuleList);

	}

	public GeneralRule(List<DateRule> dateRuleList,
			List<WeekDayRule> weekDayRuleList, List<MonthRule> monthRuleList,
			List<QuarterRule> quarterRuleList) {

		this.dateRuleList = dateRuleList;
		this.weekDayRuleList = weekDayRuleList;
		this.monthRuleList = monthRuleList;
		this.quarterRuleList = quarterRuleList;
	}

	@Override
	public String toString() {
		if (!this.dateRuleList.isEmpty()) {
			return StringUtils.join(this.dateRuleList.iterator(), ",") + "&"
					+ DateRule.RULE_TYPE;
		} else if (!this.weekDayRuleList.isEmpty()) {
			return StringUtils.join(this.weekDayRuleList.iterator(), ",") + "&"
					+ WeekDayRule.RULE_TYPE;
		} else if (!this.monthRuleList.isEmpty()) {
			return StringUtils.join(this.monthRuleList.iterator(), ",") + "&"
					+ MonthRule.RULE_TYPE;
		} else {
			return StringUtils.join(this.quarterRuleList.iterator(), ",") + "&"
					+ QuarterRule.RULE_TYPE;
		}
	}

	public static void main(String[] args) {
	}
}
