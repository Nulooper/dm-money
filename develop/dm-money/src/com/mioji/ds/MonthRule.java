package com.mioji.ds;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:57:31
 *
 */
public class MonthRule extends TicketRule {

	public static final String RULE_TYPE = "MONTH_RULE";

	private MiojiPair monthPair;

	public static MonthRule createInstance(MiojiPair monthPair, String price,
			String unit) {
		return new MonthRule(monthPair, price, unit);
	}

	public MonthRule(MiojiPair monthPair, String price, String unit) {
		super(price, unit);
		this.monthPair = monthPair;
	}

	public MiojiPair getMonthPair() {
		return monthPair;
	}

	@Override
	public String toString() {
		return this.monthPair.toString() + ":" + this.getPrice() + "-"
				+ this.getUnit();
	}

	public static void main(String[] args) {
	}

}
