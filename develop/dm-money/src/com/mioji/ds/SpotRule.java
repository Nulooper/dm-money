package com.mioji.ds;

/**
 * 描述一个景点的票价规则.仅包含成人价格或者只有一种价格.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:44:03
 *
 */
public class SpotRule {

	public static final String NULL_TICKET_TYPE = "NULL_RULE";
	public static final String UNREG_TICKET_TYPE = "UNREG_RULE";

	public static final String FREE_TICKET_TYPE = "FREE_RULE";

	public static final String DAY_TICKET_TYPE = "DAY_TICKET_RULE";
	public static final String WEEK_TICKET_TYPE = "WEEK_TICKET_RULE";
	public static final String MONTH_TICKET_TYPE = "MONTH_TICKET_RULE";
	public static final String TICKET_TYPE = "TICKET_RULE";

	public static final String ID_RULE_DELIM = "\t";
	public static final String OPEN_CLOSE_DELIM = "|";
	public static final String OPEN_DELIM = "--";
	public static final String RULE_TYPE_DELIM = "&";

	public static final String DEFAULT_CLOSE_PART = "NO" + RULE_TYPE_DELIM
			+ "NO";
	public static final String DEFAULT_NULL_RULE = "NULL" + RULE_TYPE_DELIM
			+ NULL_TICKET_TYPE + OPEN_CLOSE_DELIM + "NULL" + RULE_TYPE_DELIM
			+ NULL_TICKET_TYPE;
	public static final String DEFAULT_UNREG_RULE = "UNREG" + RULE_TYPE_DELIM
			+ UNREG_TICKET_TYPE + OPEN_CLOSE_DELIM + "UNREG" + RULE_TYPE_DELIM
			+ UNREG_TICKET_TYPE;
	public static final String DEFAULT_FREE_RULE = "FREE" + RULE_TYPE_DELIM
			+ FREE_TICKET_TYPE + OPEN_CLOSE_DELIM + "FREE" + RULE_TYPE_DELIM
			+ FREE_TICKET_TYPE;

	private String id;
	private GeneralRule generalRule;
	private SpecialRule specialRule;

	public static SpotRule createInstance(String id, GeneralRule generalRule,
			SpecialRule specialRule) {
		return new SpotRule(id, generalRule, specialRule);
	}

	public SpotRule(String id, GeneralRule generalRule, SpecialRule specialRule) {
		this.id = id;
		this.generalRule = generalRule;
		this.specialRule = specialRule;
	}

	public static String getNull(String id) {
		return id + "\t" + DEFAULT_NULL_RULE + "|" + "UNSURE";
	}

	public static String getFree(String id) {
		return id + "\t" + DEFAULT_FREE_RULE + "|" + "SURE";
	}

	public static String getOnlyOneTicket(String id, String price, String unit) {

		return id + "\t" + "{" + price + "-" + unit + "}" + RULE_TYPE_DELIM
				+ TICKET_TYPE + OPEN_CLOSE_DELIM + DEFAULT_CLOSE_PART + "|"
				+ "SURE";
	}

	public static String getUnrecog(String id) {
		return id + "\t" + DEFAULT_UNREG_RULE + "|" + "UNSURE";
	}

	@Override
	public String toString() {
		return this.id + "\t" + this.generalRule.toString() + "|"
				+ this.specialRule.toString();
	}

	public static void main(String[] args) {
		
	}

}
