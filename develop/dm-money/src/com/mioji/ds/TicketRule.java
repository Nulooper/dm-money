package com.mioji.ds;

/**
 * 票价规则基类.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:28:44
 *
 */
public class TicketRule {
	/*
	 * 单价.
	 */
	private String price;
	/*
	 * 单位.
	 */
	private String unit;

	public TicketRule(String price, String unit) {
		this.price = price;
		this.unit = unit;
	}

	public String getPrice() {
		return this.price;
	}

	public String getUnit() {
		return this.unit;
	}
}
