package com.mioji.ds;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午3:29:45
 *
 */
public class WeekDayRule extends TicketRule {
	public static final String RULE_TYPE = "WEEK_DAY_RULE";

	private MiojiPair weekDayPair;

	public static WeekDayRule createInstance(MiojiPair weekDayPair,
			String price, String unit) {

		return new WeekDayRule(weekDayPair, price, unit);
	}

	public WeekDayRule(MiojiPair weekDayPair, String price, String unit) {
		super(price, unit);
		this.weekDayPair = weekDayPair;
	}

	public MiojiPair getWeekDayPair() {
		return weekDayPair;
	}

	@Override
	public String toString() {
		return this.weekDayPair.toString() + ":" + this.getPrice() + "-"
				+ this.getUnit();
	}

	public static void main(String[] args) {
	}

}
