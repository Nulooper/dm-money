package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class AdultChfParser extends Parser {
	/*
	 * 识别模式: adult6chf
	 */
	private static final Pattern ADULT_CHF_PAT_1 = Pattern
			.compile("(adult|)(\\d{1,}|\\d{1,}\\.\\d{1,})chf$");

	/*
	 * 
	 */
	@SuppressWarnings("unused")
	private static final Pattern ADULT_CHF_PAT_2 = Pattern.compile("");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CHF_PAT_1.matcher(stt.getTicketText());

		return matcher1.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractByGroup(
				stt.getTicketText(), "\\d{1,}(\\.\\d{1,}|)");

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "chf");
	}
	
	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		AdultChfParser op = new AdultChfParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
