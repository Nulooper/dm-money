package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class AdultChildGbpParser extends Parser  {

	/*
	 * 识别模式:
	 */
	private static final Pattern ADULT_CHILD_PAT1 = Pattern
			.compile("^adult/child(gbp|)(\\d{1,}\\.\\d{1,}|\\d{1,})(gbp|)/(gbp|)(\\d{1,}|\\d{1,}\\.\\d{1,}|free)(gbp|)$");

	/*
	 * 匹配模式:"adult3eur,child2eur" 或者 "adult3eur,child免费",
	 * adult,5eur;child,3.5eur
	 */
	private static final Pattern ADULT_CHILD_PAT2 = Pattern
			.compile("adult[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,})gbp[,;]?child[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,}|免费)(gbp|)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CHILD_PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = ADULT_CHILD_PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "gbp");
	}
	
	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		AdultChildGbpParser op = new AdultChildGbpParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
