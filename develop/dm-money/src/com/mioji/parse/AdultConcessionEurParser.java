package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午1:10:32
 *
 */
public class AdultConcessionEurParser extends Parser {
	/*
	 * 识别模式: adult/concessioneur2.60/1.30 或者 adult/concession3/1.50eur
	 */
	private static final Pattern ADULT_CONCESSION_PAT1 = Pattern
			.compile("^adult/concession(eur|)(\\d{1,}\\.\\d{1,}|\\d{1,})(eur|)/(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)$");

	/*
	 * 识别模式: adult:6eurconcession:4eur 或者 adult:6eur折扣:4eur 或者
	 * adult:3.5eurconcession:3eur
	 */
	private static final Pattern ADULT_CONCESSION_PAT2 = Pattern
			.compile("^(adult|)[:]?(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)[,;]?(concession|折扣)[:]?(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CONCESSION_PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = ADULT_CONCESSION_PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "eur");
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		AdultConcessionEurParser op = new AdultConcessionEurParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
