package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class AdultParser extends Parser {
	private static String PREFIX_PAT = createORPat("dict/prefix.dict");

	private static String CURRENCY_PAT = null;
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"dict/currency.list"));
			int size = lines.size();
			StringBuilder sb = new StringBuilder();
			sb.append("(");
			int i;
			for (i = 0; i < size - 1; ++i) {
				sb.append(lines.get(i)).append("|");
			}
			sb.append(lines.get(i));
			sb.append("|");
			sb.append(")");
			CURRENCY_PAT = sb.toString();
		} catch (IOException e) {

		}
	}

	private static final String PRICE_NUMBER_PAT = "(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})";

	public static final Pattern START_PAT = Pattern.compile("^" + "("
			+ PREFIX_PAT + "|)(" + CURRENCY_PAT + PRICE_NUMBER_PAT + "|"
			+ PRICE_NUMBER_PAT + CURRENCY_PAT + ")");

	@Override
	public boolean check(SpotTicketText stt) {
		return false;
	}

	@Override
	public String doParse(SpotTicketText stt) {
		return null;
	}

	private static String createORPat(String srcPath) {

		StringBuilder sb = new StringBuilder();
		sb.append("");
		try {
			List<String> lines = FileUtils.readLines(new File(srcPath));
			int size = lines.size();
			int i;
			for (i = 0; i < size - 1; ++i) {
				sb.append(lines.get(i)).append("|");
			}
			sb.append(lines.get(i));
		} catch (IOException e) {
			e.printStackTrace();
		}
		sb.append("");
		return sb.toString();
	}
}
