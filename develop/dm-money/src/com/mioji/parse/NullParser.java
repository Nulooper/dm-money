package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

/**
 * 处理景点票价信息未知的情况:票价信息为NULL, 票价信息为空字符串两种情况. 注意: 在预处理过程中，对于票价信息为空字符串的情况，自动添加NULL.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午1:56:41
 *
 */
public class NullParser extends Parser {

	private static List<String> unknownList = null;
	static {
		try {
			unknownList = FileUtils.readLines(new File("dict/unknown.dict"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean check(SpotTicketText stt) {
		boolean matched = false;
		String ticketInfo = stt.getTicketText();
		for (String pat : unknownList) {
			matched = matched || (ticketInfo.equals(pat));
		}
		return matched;
	}

	@Override
	public String doParse(SpotTicketText stt) {
		return SpotRule.getNull(stt.getId());
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		NullParser op = new NullParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
