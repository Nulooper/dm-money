package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

/**
 * 匹配模式:eur5.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @上午10:06:47
 *
 */
public class OnlyEURParser extends Parser {

	private static final Pattern EUR_NUMBER_PAT_1 = Pattern
			.compile("^(admission|)eur\\d{1,}(\\.\\d{1,}|)$");

	private static final Pattern NUMBER_EUR_PAT_2 = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?\\d{1,}(\\.\\d{1,}|)eur(左右|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		String ticketText = stt.getTicketText();

		Matcher matcher1 = EUR_NUMBER_PAT_1.matcher(ticketText);
		Matcher matcher2 = NUMBER_EUR_PAT_2.matcher(ticketText);

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> ticketList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), ticketList.get(0), "eur");
	}
	
	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		OnlyEURParser op = new OnlyEURParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
