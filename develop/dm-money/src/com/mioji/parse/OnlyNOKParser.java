package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class OnlyNOKParser extends Parser {
	private static final Pattern NUMBER_NOK_PAT = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?\\d{1,}(\\.\\d{1,}|)nok(左右|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = NUMBER_NOK_PAT.matcher(stt.getTicketText());

		return matcher.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());
		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "nok");
	}
	
	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		OnlyNOKParser op = new OnlyNOKParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
