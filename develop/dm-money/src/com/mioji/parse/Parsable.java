package com.mioji.parse;

/**
 * 景点价格解析接口
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午2:06:43
 *
 */
public interface Parsable {
	public boolean check(SpotTicketText stt);

	public String doParse(SpotTicketText stt);

	public String getName();
}
