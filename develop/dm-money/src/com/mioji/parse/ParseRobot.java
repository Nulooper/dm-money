package com.mioji.parse;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * 
 * @author wangzhiwei
 *
 */
public class ParseRobot {

	public static void excute(String srcPath, String rulePath, String regPath,
			String unregPath) {
		try {
			System.out.println("处理开始!");
			long start = System.currentTimeMillis();
			List<String> rawLines = FileUtils.readLines(new File(srcPath));
			System.out.println("读取景点价格描述文件完毕!");
			PipeLine pl = PipeLine.createInstance();
			for (String rawText : rawLines) {
				pl.parse(rawText);
			}
			System.out.println("解析完毕!");
			System.out.println("解析成功率:\t" + pl.getRecogRate() + "!");
			FileUtils.writeLines(new File(rulePath), pl.getRuleList());
			System.out.println("解析价格规则已经写入文件[" + rulePath + "]");
			FileUtils.writeLines(new File(regPath), pl.getRecogList());
			System.out.println("被解析的原始文本已经写入文件[" + regPath + "]");
			FileUtils.writeLines(new File(unregPath), pl.getUnrecogList());
			System.out.println("没有被解析的原始文本已经写入文件[" + unregPath + "]");
			long end = System.currentTimeMillis();
			System.out.println("处理完毕!花费时间:[" + (end - start) * 1.0 / 1000
					+ "秒!]");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ParseRobot.excute("data/combine_ticket.txt", "test/rule.list",
				"test/recog.list", "test/unrecog.lis");
	}
}
