package com.mioji.parse;

public abstract class Parser implements Parsable {

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
