package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHelper {
	/**
	 * 根据regex从输入文本中抽取匹配的全部子串.
	 * 
	 * @param inputText
	 * @param regex
	 * @return
	 */
	public static List<String> extractByGroup(String inputText, String regex) {
		if (null == inputText || inputText.isEmpty() || null == regex
				|| regex.isEmpty()) {

			Logger.getGlobal().logp(Level.WARNING, RegexHelper.class.getName(),
					"extract", "输入文本或者正则表达式不合法！");
			return Collections.emptyList();
		}
		List<String> subStrs = new ArrayList<String>();
		Pattern subPat = Pattern.compile(regex);
		Matcher matcher = subPat.matcher(inputText);

		while (matcher.find()) {
			subStrs.add(matcher.group());
			// System.out.println(matcher.group());
		}

		return subStrs;
	}

	public static List<String> extractPriceList(String ticketText) {
		String regex = "\\d{1,}(\\.\\d{1,}|)";

		return extractByGroup(ticketText, regex);
	}

	public static void main(String[] args) {
		String text = "adult3eur,child0.0";
		List<String> groups = RegexHelper.extractByGroup(text,
				"\\d{1,}(\\.\\d{1,}|)");
		for (String group : groups) {
			System.out.println(group);
		}
	}
}
