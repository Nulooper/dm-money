package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

public class XAdultNOKParser extends Parser {
	private static final Pattern ADULT_NOK_PAT = Pattern
			.compile("(adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:]?(nok(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})nok)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = ADULT_NOK_PAT.matcher(stt.getTicketText());
		return matcher.find();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> subGroups = RegexHelper.extractByGroup(
				stt.getTicketText(), ADULT_NOK_PAT.toString());
		List<String> priceList = RegexHelper.extractPriceList(subGroups.get(0));

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "nok");
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		XAdultNOKParser op = new XAdultNOKParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}
}
