package com.mioji.parse2;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;
import com.mioji.parse.Parsable;
import com.mioji.parse.RegexHelper;
import com.mioji.parse.SpotTicketText;
import com.mioji.utils.Utils;

public class OnlyParser implements Parsable {
	private static Pattern PAT1 = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?" + "("
					+ Utils.CURRENCY_SUB_PAT_STR + ")"
					+ "\\d{1,}(\\.\\d{1,}|)$");
	private static Pattern PAT2 = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?\\d{1,}(\\.\\d{1,}|)"
					+ "(" + Utils.CURRENCY_SUB_PAT_STR + ")" + "$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		String ticketText = stt.getTicketText();
		List<String> ticketList = RegexHelper.extractPriceList(ticketText);

		List<String> subPatterns = RegexHelper.extractByGroup(ticketText,
				Utils.CURRENCY_SUB_PAT_STR);

		return SpotRule.getOnlyOneTicket(stt.getId(), ticketList.get(0),
				subPatterns.get(0));
	}

	@Override
	public String getName() {
		return null;
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		OnlyParser op = new OnlyParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}
}
