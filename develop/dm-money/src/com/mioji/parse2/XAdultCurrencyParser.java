package com.mioji.parse2;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;
import com.mioji.parse.Parsable;
import com.mioji.parse.RegexHelper;
import com.mioji.parse.SpotTicketText;
import com.mioji.utils.Utils;

public class XAdultCurrencyParser implements Parsable {

	private static final Pattern PAT = Pattern
			.compile("(adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:,;]?(" + "("
					+ Utils.CURRENCY_SUB_PAT_STR + ")"
					+ "(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})"
					+ "(" + Utils.CURRENCY_SUB_PAT_STR + ")" + ")");

	// (adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:]?(huf(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})huf)

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = PAT.matcher(stt.getTicketText());

		return matcher.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> subGroups = RegexHelper.extractByGroup(
				stt.getTicketText(), PAT.toString());
		List<String> priceList = RegexHelper.extractPriceList(subGroups.get(0));
		List<String> subPatterns = RegexHelper.extractByGroup(subGroups.get(0),
				Utils.CURRENCY_SUB_PAT_STR);
		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0),
				subPatterns.get(0));
	}

	@Override
	public String getName() {
		return null;
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"data/no_null_combine_ticket.txt"));
		SpotTicketText stt = null;
		int count = 0;
		XAdultCurrencyParser op = new XAdultCurrencyParser();
		for (String line : inputLines) {
			stt = new SpotTicketText(line);
			if (op.check(stt)) {
				++count;
				System.out.println(line);
				System.out.println(op.doParse(stt));
			}
		}

		System.out.println("识别数量:[" + count + "]");
		System.out.println("总共数量:[" + inputLines.size() + "]");
	}

}
