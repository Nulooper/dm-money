package com.mioji.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class Test {
	private static String CURRENCY = null;
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"dict/currency.list"));
			int size = lines.size();
			String sb = "";
			int i;
			for (i = 0; i < size - 1; ++i) {
				sb = sb + lines.get(i) + "|";
			}
			sb = sb + lines.get(i);
			CURRENCY = sb;
		} catch (IOException e) {

		}
	}

	private static final Pattern START_PAT = Pattern.compile("^((" + CURRENCY
			+ ")(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})("
			+ CURRENCY + ")).*");

	public static void createNoNullCombine(String srcPath,
			String tarNoNullPath, String tarNullPath) throws IOException {
		if (null == srcPath || null == tarNoNullPath || srcPath.isEmpty()
				|| tarNoNullPath.isEmpty()) {
			return;
		}
		List<String> srcLines = FileUtils.readLines(new File(srcPath));
		List<String> tarLines = new ArrayList<String>();
		List<String> nullLines = new ArrayList<String>();
		for (String line : srcLines) {
			String[] items = line.split("\\t");
			if (items.length != 3) {
				System.out.println("输入规则不合法![" + line + "]");
				continue;
			}
			if (!"NULL".equals(items[2]) && !items[2].isEmpty()) {
				tarLines.add(line);
			} else {
				nullLines.add(line);
			}
		}

		FileUtils.writeLines(new File(tarNoNullPath), tarLines);
		FileUtils.writeLines(new File(tarNullPath), nullLines);
	}

	public static void main(String[] args) throws IOException {
		createNoNullCombine("data/combine_ticket.txt",
				"data/no_null_combine_ticket.txt",
				"data/null_combine_ticket.txt");
	}

}
