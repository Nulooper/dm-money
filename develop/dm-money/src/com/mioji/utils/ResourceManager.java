package com.mioji.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.MiojiPair;

public class ResourceManager {

	public static final Map<String, List<MiojiPair>> CURRENCY_SAME_MAP = new HashMap<String, List<MiojiPair>>();
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"dict/cucurreny_same.dict"));
			for (String line : lines) {
				if (line.startsWith("#")) {
					continue;
				}
				List<MiojiPair> pairList = new ArrayList<MiojiPair>();
				String[] items = line.split("\t");
				String shortName = items[0];
				int length = items.length;
				for (int i = 1; i < length; ++i) {
					String[] twoItems = items[i].split("-");
					pairList.add(MiojiPair.createInstance(twoItems[0],
							twoItems[1]));
				}
				CURRENCY_SAME_MAP.put(shortName, pairList);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
