package com.mioji.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public class Utils {

	public static String CURRENCY_SUB_PAT_STR = null;
	static {
		try {
			List<String> currencyList = FileUtils.readLines(new File(
					"dict/currency.list"));
			CURRENCY_SUB_PAT_STR = Utils.join(currencyList, "|");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String join(List<String> inputList, String delim) {
		if (null == inputList || inputList.isEmpty()) {
			return "";
		}
		if (null == delim) {
			delim = " ";
		}

		int size = inputList.size();
		int i = 0;
		StringBuilder sb = new StringBuilder();
		for (; i < size - 1; ++i) {
			sb.append(inputList.get(i)).append(delim);
		}
		sb.append(inputList.get(i));

		return sb.toString();
	}

	public static void main(String[] args) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(
				"dict/currency.list"));
		Set<String> currencySet = new HashSet<String>();
		for (String line : inputLines) {
			currencySet.add(line.trim());
		}

		FileUtils.writeLines(new File("dict/currency.set"),
				new ArrayList<String>(currencySet));
	}
}
