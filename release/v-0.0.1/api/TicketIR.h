/////////////////////////////////////////////////////////////
/*Filename   : TicketIR.h                                        */
/*Create Date: 2014-07-011                                  */
/*Author     : Wang Zhiwei 王志伟                                */
/////////////////////////////////////////////////////////////
#ifndef _TICKET_IR_H_
#define _TICKET_IR_H_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

const static std::string RETURN_UNRECOG = "0";
const static std::string RETURN_UNKNOWN = "0";
const static std::string RETURN_FREE = "0";

const static std::string RETURN_CLOSE = "0";

const static std::string RULE_DELIM = "|";

const static std::string ONLY_TYPE = "ONE";
const static std::string TYPE_UNKNOWN = "UKNOWN";
const static std::string TYPE_UNRECOG = "UNRECOG";

class TicketIR
{
public:
    static bool search(const std::string& rule, const std::string& in_date, std::string& out_tickets);
    
public:
    
    static void vector2String(const std::vector<std::string>& time_pair_vec, const std::string& delim, std::string& out_str);
    static void split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec);
    static std::string int2String(int in_number);
    static int string2int(const std::string& in_str);
   // static int getWDay(const time_t& t, int zone = 8);
    //static time_t toTime(const std::string& s, int zone = 8);
    
   // static bool checkDayOfWeek(const std::string& in_day_of_week, const std::string& special_type, const std::string& open_week_time_rule,
                               //const std::string& close_week_time_rule, std::string& out_tickets);
   // static std::string getDayOfWeek(const std::string& in_date);
    //static bool getDayOfWeekTimesMap(const std::string& week_time_rule, std::map<std::string, std::vector<std::string> >& out_map);
    //static void spitWeekTimeRule(const std::string& week_time_rule, std::vector<std::string>& out_vec);
    
    //static bool checkTime(const std::string& day_of_week, const std::string& special_type, const std::string& general_text, const std::string& close_text,
                          //std::string& out_tickets);
    //static void splitTimeRule(const std::string& time_rule, std::vector<std::string>& out_vec);
    
   // static bool checkMonthTime(int month_of_given_day, const std::string& special_type, const std::string& general_text, const std::string& close_text,
                              // std::string& out_tickets);
   // static int getMonthOfDay(const std::string& in_date);
   // static void getMonthTimeMap(const std::string& month_time_rule, std::map<int, std::string>& out_map);
    
};

bool TicketIR::search(const std::string& rule, const std::string& in_date, std::string& out_tickets)
{
	std::vector<std::string> rule_items;
    split(rule, RULE_DELIM, rule_items);
	if(rule_items.empty())
	{
		out_tickets = RETURN_UNKNOWN;
		return false;
	}
    
	std::string general_rule = rule_items[0];
	//std::cerr << "general_rule:[" << general_rule << "]" << std::endl;
	std::vector<std::string> open_items;
    split(general_rule, "&", open_items);
	std::string general_text = open_items[0];
	std::string general_type = open_items[1];
	//std::cerr << "general_text:[" << general_text << "] general_type:[" << general_type << "]" << std::endl;
    
    std::string special_rule = rule_items[1];
   // std::cerr << "special_rule:[" << special_rule << "]" << std::endl;
    std::vector<std::string> close_items;
    split(special_rule, "&", close_items);
    std::string special_text = close_items[0];
    std::string special_type = close_items[1];
    //std::cerr << "special_text:[" << special_text << "] special_type:[" << special_type << "]" << std::endl;
    
    //std::cerr << "general_text[" << general_text << "] general_type[" << general_type << "] close_text["
    //<< close_text << "] special_type["<< special_type << "]" << std::endl;
    
    if(TYPE_UNKNOWN == general_type && TYPE_UNKNOWN == special_type)
    {
    	out_tickets = RETURN_UNKNOWN;
    	std::cerr << "match TYPE_UNKNOWN" << std::endl;
    	return true;
    }
    else if(TYPE_UNRECOG == general_type && TYPE_UNRECOG == special_type)
    {
    	out_tickets = RETURN_UNRECOG;
    	std::cerr << "match TYPE_UNRECOG" << std::endl;
    	return true;
    }
    else if(ONLY_TYPE == general_type)
    {
        
    	out_tickets = general_text;
    	std::cerr << "macth ONLY_TYPE! out_tickets:[" << out_tickets << "]" << std::endl;
    	return true;
    }
    else
    {
    	/*std::string day_of_week = getDayOfWeek(in_date);
    	//cerr<<" "<<day_of_week<<" ";
    	int month_of_day = getMonthOfDay(in_date);
    	if(TYPE_WEEK_TIME == general_type)
    	{
            //cerr << "macth week_time" << endl;
            //std::cerr << "in_date:[" << in_date << "] day_of_week:[" << day_of_week << "] month_of_day:[" << month_of_day << "]" << std::endl;
    		return checkDayOfWeek(day_of_week, special_type, general_text, close_text, out_tickets);
    	}
    	else if(TYPE_ONLY_TIME == general_type)
    	{
            //cerr << "macth only_time" << endl;
    		return checkTime(day_of_week, special_type, general_text, close_text, out_tickets);
    	}
    	else if(TYPE_MONTH_TIME == general_type)
    	{
            //cerr << "match month_time" << endl;
    		return checkMonthTime(month_of_day, special_type, general_text, close_text, out_tickets);
    	}
    	else
    	{
            //cerr << "match nothing" << endl;
    		out_tickets = RETURN_UNKNOWN;
    		return false;
    	}*/
    		out_tickets = RETURN_UNKNOWN;
    
    return false;
    }
    //cerr << "macth nothing !" << endl;
    
}

void TicketIR::vector2String(const std::vector<std::string>& time_pair_vec, const std::string& split, std::string& out_str)
{
	std::vector<std::string>::size_type size = time_pair_vec.size();
    
    if(0 == size)
    {
    	out_str = "";
    	return;
    }
    
    if(1 == size){
        out_str = time_pair_vec[0];
        return;
    }
    
    out_str = "";
    for(std::vector<std::string>::size_type i = 0; i < size - 1; ++i)
    {
        out_str += time_pair_vec[i] + split;
    }
    
    out_str += time_pair_vec[size - 1];
}

void TicketIR::split(const std::string& in_str, const std::string& delim, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
    std::string::size_type pos = 0;
    std::string::size_type len = in_str.length();
    std::string::size_type delim_len = delim.length();
    if (0 == len || 0 == delim_len)
    {
    	return;
    }
    while (pos < len)
    {
        std::string::size_type find_pos = in_str.find(delim, pos);
        if (find_pos == -1)
        {
            out_vec.push_back(in_str.substr(pos, len - pos));
            break;
        }
        out_vec.push_back(in_str.substr(pos, find_pos - pos));
        pos = find_pos + delim_len;
    }
}

std::string TicketIR::int2String(int in_number)
{
    std::stringstream ss;
	ss << in_number;
    std::string number_str;
	ss >> number_str;
    
	return number_str;
}

int TicketIR::string2int(const std::string& in_str)
{
    std::stringstream ss;
	ss << in_str;
	int number;
	ss >> number;
    
	return number;
}

/*time_t TicketIR::toTime(const std::string& s, int zone){
	struct tm TM;
    strptime(s.c_str(),"%Y%m%d_%H:%M",&TM);
    TM.tm_sec = 0;

    //std::cerr<<TM.tm_year<<" "<<TM.tm_mon<<" "<<TM.tm_mday<<" "<<TM.tm_hour<<" "<<TM.tm_min<<std::endl;
    time_t ret = mktime(&TM)+TM.tm_gmtoff-zone*3600;
    return ret;
}

int TicketIR::getWDay(const time_t& t, int zone){
	struct tm TM;
	time_t nt = t + zone * 3600;
	gmtime_r(&nt,&TM);
	int ret = (TM.tm_wday + 6) % 7 + 1;
	return ret;
}



///////////////////////////////////
//week time rule.

bool TicketIR::checkDayOfWeek(const std::string& in_day_of_week, const std::string& special_type, const std::string& open_week_time_rule,
                        const std::string& close_week_time_rule, std::string& out_tickets)
{
	//std::cerr << "in_day_of_week:[" << in_day_of_week << "]" << std::endl;
	//wait to implement.
	if(TYPE_WEEK_TIME == special_type)
	{
		std::map<std::string, std::vector<std::string> > close_map;
		getDayOfWeekTimesMap(close_week_time_rule, close_map);
		if(close_map.find(in_day_of_week) != close_map.end())
		{
            out_tickets = RETURN_CLOSE;
            return true;
		}
        
	}
	std::map<std::string, std::vector<std::string> > open_map;
	getDayOfWeekTimesMap(open_week_time_rule, open_map);
	if(open_map.find(in_day_of_week) == open_map.end())
	{
		out_tickets = RETURN_CLOSE;
		return true;
	}
    
	vector2String(open_map[in_day_of_week], ",", out_tickets);
	return true;
}

std::string TicketIR::getDayOfWeek(const std::string& in_date)
{
	
	time_t tt = IR::toTime(in_date+"_00:00");
    int wd = IR::getWDay(tt);

	return "周" + int2String(wd);
	//return "周1"; 
}


bool TicketIR::getDayOfWeekTimesMap(const std::string& week_time_rule, std::map<std::string, std::vector<std::string> >& out_map)
{
	//wait to implement.
	std::vector<std::string> week_time_vec;
	spitWeekTimeRule(week_time_rule, week_time_vec);
    
	out_map.clear();
	for(std::vector<std::string>::iterator it = week_time_vec.begin(); it != week_time_vec.end(); ++it)
	{
		std::string week_time = *it;
                //std::cerr << "week_time size:[" << week_time.size() << "]["<< week_time << "]" << std::endl;
		int index1 = week_time.find("-");
		int index2 = week_time.find(":");
		std::string start_day = week_time.substr(0, index1);
		std::string end_day = week_time.substr(index1 + 1, index2 - index1 - 1);
        
		//std::cerr << "start_day[" << start_day << "] end_day[" << end_day << "]" << std::endl;
        
		std::string time_info = week_time.substr(index2 +1 , week_time.size() - index2 - 1);
		//std::cerr << "time_info[" << time_info << "]" << std::endl;
		std::vector<std::string> time_pair_vec;
		split(time_info, ",", time_pair_vec);
		if(start_day == end_day)
		{
			out_map[start_day] = time_pair_vec;
			//cerr << "start_day[" << start_day << "]" << endl;
		}
		else
		{
			int start = string2int(start_day.substr(3, 1));
			int end = string2int(end_day.substr(3, 1));
                        //std::cerr << start_day.substr(3) << "-" << end_day.substr(3) << std::endl;
                       //std::cerr << "start[" << start << "] end[" << end << "]" << std::endl;
			if(start < end)
			{
				for(int i = start; i <= end; ++i)
				{
					out_map["周" + int2String(i)] = time_pair_vec;
				}
			}
			else
			{
				for(int j = start; j <= 7; ++j)
				{
					out_map["周" + int2String(j)] = time_pair_vec;
				}
				for(int m = 1; m <= end; ++m)
				{
					out_map["周" + int2String(m)] = time_pair_vec;
				}
			}
		}
	}
	return true;
}

void TicketIR::spitWeekTimeRule(const std::string& week_time_rule, std::vector<std::string>& out_vec)
{
       // std::cerr << "in spitWeekTimeRule week_time_rule[" << week_time_rule << "]" << std::endl;
	std::vector<std::string> time_vec;
	split(week_time_rule, "{", time_vec);
    
	out_vec.clear();
    
	for(std::vector<std::string>::iterator iter = time_vec.begin(); iter != time_vec.end(); ++iter)
	{
        std::string time = *iter;
		if(time.empty())
		{
			continue;
		}
		out_vec.push_back(time.substr(0, time.find("}")));
	}
}

///////////////////////////////////
//time rule.

bool TicketIR::checkTime(const std::string& day_of_week, const std::string& special_type, const std::string& general_text, const std::string& close_text,
                   std::string& out_tickets)
{
	std::vector<std::string> time_vec;
	splitTimeRule(general_text, time_vec);
	if(TYPE_WEEK_TIME == special_type)
	{
		std::map<std::string, std::vector<std::string> > close_map;
		getDayOfWeekTimesMap(close_text, close_map);
		if(close_map.find(day_of_week) != close_map.end())
		{
			out_tickets = RETURN_CLOSE;
			return true;
		}
		vector2String(time_vec, ",", out_tickets);
		return true;
	}
	else if ("NO" == special_type)
	{
		vector2String(time_vec, ",", out_tickets);
		return true;
	}
	else
	{
		out_tickets = RETURN_UNKNOWN;
		return true;
	}
}

void TicketIR::splitTimeRule(const std::string& time_rule, std::vector<std::string>& out_vec)
{
	out_vec.clear();
    
	std::string cleaned_time_rule = time_rule.substr(1, time_rule.find("}") - 1);
	split(cleaned_time_rule, ",", out_vec);
}

/////////////////////////////////
//month time rule.

bool TicketIR::checkMonthTime(int month_of_given_day, const std::string& special_type, const std::string& general_text, const std::string& close_text,
                        std::string& out_tickets)
{
	std::map<int, std::string> open_map;
	getMonthTimeMap(general_text, open_map);
    
	if("NO" == special_type)
	{
		if(open_map.find(month_of_given_day) != open_map.end())
		{
			out_tickets = open_map[month_of_given_day];
			return true;
		}
		out_tickets = RETURN_CLOSE;
		return true;
	}
    
	if(open_map.find(month_of_given_day) != open_map.end())
	{
		out_tickets = open_map[month_of_given_day];
		return true;
	}
	out_tickets = RETURN_CLOSE;
	return true;
}

int TicketIR::getMonthOfDay(const std::string& in_date)
{
	std::string month = in_date.substr(4, 2);
    
	return string2int(month);
}

void TicketIR::getMonthTimeMap(const std::string& month_time_rule, std::map<int, std::string>& out_map)
{
	int index1 = month_time_rule.find("月");
	int index2 = month_time_rule.find("-");
	int index3 = month_time_rule.find(":");
	std::string start_month = month_time_rule.substr(1, index1 - 1);
	std::string end_month = month_time_rule.substr(index2 + 1, month_time_rule.rfind("月") - index2 - 1);
	int start = string2int(start_month);
	int end = string2int(end_month);
	std::string time_pair = month_time_rule.substr(index3 + 1, month_time_rule.rfind("}") - index3 - 1);
    
	out_map.clear();
    
	if(start <= end)
	{
		for(int i = start; i <= end; ++i)
		{
			out_map[i] = time_pair;
		}
	}
	else
	{
		for(int j = start; j <= 12; ++j)
		{
			out_map[j] = time_pair;
		}
		for(int m = 1; m <= end; ++m)
		{
			out_map[m] = time_pair;
		}
	}
}*/

#endif

