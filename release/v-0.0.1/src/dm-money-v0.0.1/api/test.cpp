#include "TicketIR.h"
#include <fstream>
#include <time.h>

using namespace std;

void readLines(const string& filename, vector<string>& lines)
{
    lines.clear();
    ifstream fin;
    fin.open(filename.c_str(), std::ifstream::in);
    string line;
    while(getline(fin, line))
    {
        lines.push_back(line);
    }
    fin.close();
}

void writeLines(const string& filename, const vector<string>& lines)
{
    ofstream fout;
    fout.open(filename.c_str(), std::ofstream::out | std::ofstream::app);
    int size = lines.size();
    for(int i = 0; i < size; ++i)
    {
        fout << lines[i];
    }
    fout.close();
}

void testAll( const vector<string>& test_days) 
{
    vector<string> unknown_lines;
    readLines("../test/rule.list", unknown_lines);
    int size = unknown_lines.size();
    vector<string> results;
    std::vector<string> items;
    for(int i = 0; i < size; ++i)
    {
        string combine_result_line = "" + unknown_lines[i] + "\n";
        int days_size = test_days.size();
        TicketIR::split(unknown_lines[i], "\t", items);
        for(int j = 0; j < days_size; ++j)
        {
            string out_times;  
            TicketIR::search(items[1], test_days[j], out_times);
            std::cerr << "输出结果:[" << out_times << "]" << std::endl;
            combine_result_line += test_days[j]+ ": " + out_times +"\n" ;
            std::cerr << "综合结果:[" << combine_result_line + "]" << std::endl;
        }
        results.push_back(combine_result_line);
    }
    writeLines("unknown.results", results);
}



int main()
{
    vector<string> test_days;
    for(int i = 1; i <= 10; ++i)
    {
        test_days.push_back(TicketIR::int2String(20140712 + i));
        test_days.push_back(TicketIR::int2String(20140113 + i));
    }
    clock_t start, finish;
    double total_time;
    start = clock();
    testAll(test_days);
    //testTimeRule(test_days);
    
    //testWeekTimeRule(test_days);
    
    //testMonthTimeRule(test_days);
    finish = clock();
    total_time = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "程序的运行时间为:[" << total_time << "]秒!" << endl;
}
