===数据库 onlinedb

== 表的结构 city

|------
|字段|类型|空|默认
|------
|//**id**//|char(8)|否|
|name|varchar(64)|是|NULL
|name_en|varchar(64)|是|NULL
|tri_code|char(8)|是|NULL
|alias|varchar(128)|是|NULL
|py|varchar(64)|是|NULL
|continent|varchar(32)|是|NULL
|country|varchar(64)|是|NULL
|region|varchar(64)|是|NULL
|map_info|varchar(64)|是|NULL
|time_zone|int(11)|是|NULL
|grade|int(11)|是|NULL
|through|varchar(256)|是|NULL
|petrol_price|float|是|NULL
|ctrip_id|varchar(16)|是|NULL
|status|char(8)|是|NULL
== 转存表中的数据 city
