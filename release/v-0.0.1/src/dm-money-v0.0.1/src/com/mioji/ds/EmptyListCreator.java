package com.mioji.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:21:30
 *
 */
public class EmptyListCreator {
	public static List<DateRule> getEmptyDTR() {
		List<DateRule> drList = new ArrayList<DateRule>();
		return Collections.unmodifiableList(drList);
	}

	public static List<WeekDayRule> getEmptyWTR() {
		List<WeekDayRule> wdrList = new ArrayList<WeekDayRule>();
		return Collections.unmodifiableList(wdrList);
	}

	public static List<MonthRule> getEmptyMTR() {
		List<MonthRule> mrList = new ArrayList<MonthRule>();
		return Collections.unmodifiableList(mrList);
	}

	public static List<QuarterRule> getEmptyQTR() {
		List<QuarterRule> qrList = new ArrayList<QuarterRule>();
		return Collections.unmodifiableList(qrList);
	}
}
