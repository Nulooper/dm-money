package com.mioji.ds;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:17:48
 *
 */
public class MiojiPair {
	public String start;
	public String end;

	public static MiojiPair createDefaultDate() {
		return new MiojiPair("01月01日", "12月31日");
	}

	public static MiojiPair createDefaultWeekDay() {
		return new MiojiPair("周1", "周7");
	}

	public static MiojiPair createDefaultMonth() {
		return new MiojiPair("01月", "12月");
	}

	public static MiojiPair createDefaultQuarter() {
		return new MiojiPair("春季", "冬季");
	}

	public static MiojiPair createInstance(String start, String end) {
		return new MiojiPair(start, end);
	}

	public MiojiPair(String start, String end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public String toString() {
		return start + "-" + end;
	}

}
