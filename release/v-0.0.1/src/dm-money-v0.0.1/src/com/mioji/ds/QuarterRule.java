package com.mioji.ds;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:07:49
 *
 */
public class QuarterRule extends TicketRule {
	public static final String RULE_TYPE = "QUARTER_RULE";

	private MiojiPair quarterPair;

	public static QuarterRule createInstance(MiojiPair quarterPair,
			String price, String unit) {
		return new QuarterRule(quarterPair, price, unit);
	}

	public QuarterRule(MiojiPair quarterPair, String price, String unit) {
		super(price, unit);
		this.quarterPair = quarterPair;
	}

	public MiojiPair getQuarterPair() {
		return quarterPair;
	}

	@Override
	public String toString() {
		return this.quarterPair.toString() + ":" + this.getPrice() + "-"
				+ this.getUnit();
	}

	public static void main(String[] args) {
	}

}
