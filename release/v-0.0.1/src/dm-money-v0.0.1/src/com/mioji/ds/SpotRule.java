package com.mioji.ds;

/**
 * 描述一个景点的票价规则.仅包含成人价格或者只有一种价格.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午4:44:03
 *
 */
public class SpotRule {

	private String id;
	private GeneralRule generalRule;
	private SpecialRule specialRule;

	public static SpotRule createInstance(String id, GeneralRule generalRule,
			SpecialRule specialRule) {
		return new SpotRule(id, generalRule, specialRule);
	}

	public SpotRule(String id, GeneralRule generalRule, SpecialRule specialRule) {
		this.id = id;
		this.generalRule = generalRule;
		this.specialRule = specialRule;
	}

	public static String getUnknown(String id) {
		return id + "\t" + "UNKNOWN" + "&UNKNOWN" + "|" + "UNKNOWN"
				+ "&UNKNOWN";
	}

	public static String getFree(String id) {
		return id + "\t" + "FREE" + "&FREE" + "|" + "FREE" + "&FREE";
	}

	public static String getOnlyOneTicket(String id, String price, String unit) {
		return id + "\t" + price + "-" + unit + "&ONE" + "|" + "NO" + "&NO";
	}

	public static String getUnrecog(String id) {
		return id + "\t" + "UNRECOG" + "&UNRECOG" + "|" + "UNRECOG"
				+ "&UNRECOG";
	}

	@Override
	public String toString() {
		return this.id + "\t" + this.generalRule.toString() + "|"
				+ this.specialRule.toString();
	}

	public static void main(String[] args) {
	}

}
