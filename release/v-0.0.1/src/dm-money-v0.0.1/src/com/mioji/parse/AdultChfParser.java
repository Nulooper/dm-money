package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class AdultChfParser implements Parsable {
	/*
	 * 识别模式: adult6chf
	 */
	private static final Pattern ADULT_CHF_PAT_1 = Pattern
			.compile("(adult|)(\\d{1,}|\\d{1,}\\.\\d{1,})chf$");

	/*
	 * 
	 */
	@SuppressWarnings("unused")
	private static final Pattern ADULT_CHF_PAT_2 = Pattern.compile("");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CHF_PAT_1.matcher(stt.getTicketText());

		return matcher1.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractByGroup(
				stt.getTicketText(), "\\d{1,}(\\.\\d{1,}|)");

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "chf");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
