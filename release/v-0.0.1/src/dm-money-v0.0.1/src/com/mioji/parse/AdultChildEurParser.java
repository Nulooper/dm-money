package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @上午10:17:54
 *
 */
public class AdultChildEurParser implements Parsable {

	/*
	 * 匹配模式:"adult/childeur3/2.50" 或 "adult/childeur3/free"
	 */
	private static final Pattern ADULT_CHILD_PAT1 = Pattern
			.compile("^adult/child(eur|)(\\d{1,}\\.\\d{1,}|\\d{1,})(eur|)/(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,}|free)(eur|)$");

	/*
	 * 匹配模式:"adult3eur,child2eur" 或者 "adult3eur,child免费",
	 * adult,5eur;child,3.5eur
	 */
	private static final Pattern ADULT_CHILD_PAT2 = Pattern
			.compile("adult[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,})eur[,;]?child[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,}|免费)(eur|)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CHILD_PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = ADULT_CHILD_PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		String ticketText = stt.getTicketText();
		ticketText = ticketText.replaceAll("free", "0.0");
		ticketText = ticketText.replaceAll("免费", "0.0");

		List<String> priceList = RegexHelper.extractByGroup(ticketText,
				"\\d{1,}(\\.\\d{1,}|)");
		String adultPrice = priceList.get(0);
		// String childPrice = priceList.get(1);//暂时不需要

		// /wait to code.

		return SpotRule.getOnlyOneTicket(stt.getId(), adultPrice, "eur");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	public static void main(String[] args) {

	}
}
