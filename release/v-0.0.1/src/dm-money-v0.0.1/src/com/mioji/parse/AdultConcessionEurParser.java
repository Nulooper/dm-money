package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

/**
 * 
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月11日 @下午1:10:32
 *
 */
public class AdultConcessionEurParser implements Parsable {
	/*
	 * 识别模式: adult/concessioneur2.60/1.30 或者 adult/concession3/1.50eur
	 */
	private static final Pattern ADULT_CONCESSION_PAT1 = Pattern
			.compile("^adult/concession(eur|)(\\d{1,}\\.\\d{1,}|\\d{1,})(eur|)/(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)$");

	/*
	 * 识别模式: adult:6eurconcession:4eur 或者 adult:6eur折扣:4eur 或者
	 * adult:3.5eurconcession:3eur
	 */
	private static final Pattern ADULT_CONCESSION_PAT2 = Pattern
			.compile("^(adult|)[:]?(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)[,;]?(concession|折扣)[:]?(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_CONCESSION_PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = ADULT_CONCESSION_PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "eur");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	public static void main(String[] args) {
		Matcher matcher1 = ADULT_CONCESSION_PAT1
				.matcher("adult/concessioneur2.60/1.30");
		Matcher matcher2 = ADULT_CONCESSION_PAT2
				.matcher("adult:6eurconcession:4eur");
		System.out.println(matcher1.matches());
		System.out.println(matcher2.matches());
	}

}
