package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class AdultStudentChildEurParser implements Parsable {

	/*
	 * 匹配模式: adult/学生/childeur3/2/1
	 */
	private static final Pattern ADULT_STUDENT_CHILD_PAT1 = Pattern
			.compile("^adult/(学生|student)/childeur(\\d{1,}|\\d{1,}\\.\\d{1,})/(\\d{1,}|\\d{1,}\\.\\d{1,})/(\\d{1,}|\\d{1,}\\.\\d{1,})$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = ADULT_STUDENT_CHILD_PAT1.matcher(stt.getTicketText());

		return matcher.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "eur");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	public static void main(String[] args) {
		Matcher matcher = ADULT_STUDENT_CHILD_PAT1
				.matcher("adult/学生/childeur3/2/1");
		System.out.println(matcher.matches());
	}
}
