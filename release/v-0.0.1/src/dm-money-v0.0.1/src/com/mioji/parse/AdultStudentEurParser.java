package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class AdultStudentEurParser implements Parsable {
	/*
	 * 匹配模式: adult/学生eur5/3 adult/学生eur8.2/6.2
	 */
	private static final Pattern ADULT_STUDENT_PAT1 = Pattern
			.compile("^adult/(学生|student)(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)/(eur|)(\\d{1,}|\\d{1,}\\.\\d{1,})(eur|)$");

	/*
	 * 匹配模式:6eur/人,学生2.5eur/人
	 */
	private static final Pattern ADULT_STUDENT_PAT2 = Pattern
			.compile("(adult|)[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,})eur(/人|)[,;]?(学生|student)[:,]?(\\d{1,}|\\d{1,}\\.\\d{1,}|免费)(eur|)(/人|)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher1 = ADULT_STUDENT_PAT1.matcher(stt.getTicketText());
		Matcher matcher2 = ADULT_STUDENT_PAT2.matcher(stt.getTicketText());

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> priceList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "eur");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
