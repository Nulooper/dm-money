package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.mioji.ds.SpotRule;

/**
 * 处理景点免费得情况.景点票价的免费模式从字典文件中读取.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午1:59:26
 *
 */
public class FreeParser implements Parsable {

	private static List<String> freeList = null;
	static {
		try {
			freeList = FileUtils.readLines(new File("dict/free.dict"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean check(SpotTicketText stt) {
		boolean matched = false;
		String ticketInfo = stt.getTicketText();
		for (String pat : freeList) {
			matched = matched || (ticketInfo.equals(pat));
		}
		return matched;
	}

	@Override
	public String doParse(SpotTicketText stt) {
		return SpotRule.getFree(stt.getId());
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}
}
