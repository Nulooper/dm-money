package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class OnlyGBPParser implements Parsable {

	private static final Pattern NUMBER_GBP_PAT_1 = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?\\d{1,}(\\.\\d{1,}|)gbp$");

	private static final Pattern NUMBER_GBP_PAT_2 = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?gbp\\d{1,}(\\.\\d{1,}|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		String ticketText = stt.getTicketText();

		Matcher matcher1 = NUMBER_GBP_PAT_1.matcher(ticketText);
		Matcher matcher2 = NUMBER_GBP_PAT_2.matcher(ticketText);

		return matcher1.matches() || matcher2.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		String ticketText = stt.getTicketText();
		List<String> ticketList = RegexHelper.extractPriceList(ticketText);

		return SpotRule.getOnlyOneTicket(stt.getId(), ticketList.get(0), "gbp");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
