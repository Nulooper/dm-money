package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class OnlyKRParser implements Parsable {
	private static final Pattern NUMBER_KR_PAT = Pattern
			.compile("^(admission|门票|门票约|全票|adult|全价|)[:]?\\d{1,}(\\.\\d{1,}|)kr(左右|)$");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = NUMBER_KR_PAT.matcher(stt.getTicketText());

		return matcher.matches();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> ticketList = RegexHelper.extractPriceList(stt
				.getTicketText());

		return SpotRule.getOnlyOneTicket(stt.getId(), ticketList.get(0), "kr");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
