package com.mioji.parse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * Parser的创建工厂.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午1:50:38
 *
 */
public class ParserFactory {
	/**
	 * 读取配置文件创建parser.
	 * 
	 * @return a list of parsers.
	 */
	public static List<Parsable> createParsers() {
		List<Parsable> parserList = new ArrayList<Parsable>();

		try {
			List<String> parserNameList = FileUtils.readLines(new File(
					"conf/parser.list"));
			for (String classname : parserNameList) {
				parserList.add((Parsable) Class.forName(classname)
						.newInstance());
			}
		} catch (IOException | InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			e.printStackTrace();
		}
		return Collections.unmodifiableList(parserList);
	}
}
