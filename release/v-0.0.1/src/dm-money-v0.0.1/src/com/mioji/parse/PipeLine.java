package com.mioji.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mioji.ds.SpotRule;

public class PipeLine {
	private List<String> recogList;
	private List<String> unrecogList;
	private List<String> ruleList;
	private int total;

	public PipeLine() {
		this.recogList = new ArrayList<String>();
		this.unrecogList = new ArrayList<String>();
		this.ruleList = new ArrayList<String>();
		this.total = 0;
	}

	public static PipeLine createInstance() {
		return new PipeLine();
	}

	public void parse(String rawText) {
		++total;
		SpotTicketText stt = SpotTicketText.createInstance(rawText);
		List<Parsable> parserList = ParserFactory.createParsers();
		for (Parsable parser : parserList) {
			if (parser.check(stt)) {
				this.ruleList.add(parser.doParse(stt));
				this.recogList.add(stt.getTicketText());
				return;
			}
		}
		this.ruleList.add(SpotRule.getUnrecog(stt.getId()));
		this.unrecogList.add(stt.getTicketText());
	}

	public List<String> getRecogList() {
		return Collections.unmodifiableList(this.recogList);
	}

	public double getRecogRate() {
		return this.recogList.size() * 1.0 / total;
	}

	public List<String> getUnrecogList() {
		Collections.sort(this.unrecogList);
		return Collections.unmodifiableList(this.unrecogList);
	}

	public List<String> getRuleList() {
		return Collections.unmodifiableList(this.ruleList);
	}
}
