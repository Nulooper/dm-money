package com.mioji.parse;

import java.io.IOException;

/**
 * 定义景点价格文本结构. 输入的格式为: "id\t价格"
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @2014年7月10日 @下午2:58:05
 *
 */
public class SpotTicketText {
	private String id;
	private String country;
	private String ticketText;

	public SpotTicketText(String rawText) {
		this.init(rawText);
	}

	private void init(String rawText) {
		// step1: 转成小写
		rawText = rawText.toLowerCase();

		// step2: 分割id和价格文本
		String[] items = rawText.split("\t");
		this.id = items[0];
		this.country = items[1];
		this.ticketText = items[2];

		// step3: 对价格文本进行预处理.
		this.preprocess();
	}

	public static SpotTicketText createInstance(String rawText) {
		return new SpotTicketText(rawText);
	}

	public String getId() {
		return this.id;
	}

	public String getCountry() {
		return this.country;
	}

	public String getTicketText() {
		return this.ticketText;
	}

	private void preprocess() {
		this.clean1();
		this.clean2();
		this.clean3();
	}

	/**
	 * 替换和删除特殊符号.
	 */
	private void clean1() {
		// this.ticketText = this.ticketText.replaceAll(" ", "");
		this.ticketText = this.ticketText.replaceAll("，", ",");
		this.ticketText = this.ticketText.replaceAll("。", "");
		this.ticketText = this.ticketText.replaceAll("：", ":");
		this.ticketText = this.ticketText.replaceAll("；", ";");
		this.ticketText = this.ticketText.replaceAll("&ndash;", "-");
		this.ticketText = this.ticketText.replaceAll("&nbsp;", "");
		this.ticketText = this.ticketText.replaceAll("&acirc;", "");
		this.ticketText = this.ticketText.replaceAll("&not;", "");
		this.ticketText = this.ticketText.replaceAll("&acirc;", "");

		this.ticketText = this.ticketText.replaceAll("&amp;", "");
		this.ticketText = this.ticketText.replaceAll("～", "-");
		this.ticketText = this.ticketText.replaceAll("~", "-");
		this.ticketText = this.ticketText.replaceAll("——", "-");
		this.ticketText = this.ticketText.replaceAll("–", "-");
		this.ticketText = this.ticketText.replaceAll("--", "-");
		this.ticketText = this.ticketText.replaceAll("、", ",");

		this.ticketText = this.ticketText.replaceAll("&euro;", "eur");// 欧元
		this.ticketText = this.ticketText.replaceAll("€", "eur");// 欧元
		this.ticketText = this.ticketText.replaceAll("欧元", "eur");
		this.ticketText = this.ticketText.replaceAll("欧", "eur");
		this.ticketText = this.ticketText.replaceAll("euro", "eur");

		this.ticketText = this.ticketText.replaceAll("&pound;", "gbp");// 英镑
		this.ticketText = this.ticketText.replaceAll("英镑", "gbp");
		this.ticketText = this.ticketText.replaceAll("镑", "gbp");

		this.ticketText = this.ticketText.replaceAll("丹麦克朗", "dkk");
		this.ticketText = this.ticketText.replaceAll("挪威克朗", "nok");

		this.ticketText = this.ticketText.replaceAll("瑞士法郎", "chf");
		this.ticketText = this.ticketText.replaceAll("瑞郎", "chf");
		this.ticketText = this.ticketText.replaceAll("福林", "huf");

		this.ticketText = this.ticketText.replaceAll("成人票价", "adult");
		this.ticketText = this.ticketText.replaceAll("成人票", "adult");
		this.ticketText = this.ticketText.replaceAll("成人", "adult");

		this.ticketText = this.ticketText.replaceAll("儿童票价", "child");
		this.ticketText = this.ticketText.replaceAll("儿童票", "child");
		this.ticketText = this.ticketText.replaceAll("儿童", "child");

		this.ticketText = this.ticketText.replaceAll("优惠票价", "concession");
		this.ticketText = this.ticketText.replaceAll("优惠票", "concession");
		this.ticketText = this.ticketText.replaceAll("优惠", "concession");

		this.ticketText = this.ticketText.replaceAll("优待票价", "concession");
		this.ticketText = this.ticketText.replaceAll("优待票", "concession");

		this.ticketText = this.ticketText.replaceAll("concession价格",
				"concession");
		this.ticketText = this.ticketText.replaceAll("concession价",
				"concession");

		this.ticketText = this.ticketText.replaceAll("全票价", "adult");
		this.ticketText = this.ticketText.replaceAll("全价票", "adult");
		this.ticketText = this.ticketText.replaceAll("全价", "adult");

		this.ticketText = this.ticketText.replaceAll("学生票价", "student");
		this.ticketText = this.ticketText.replaceAll("学生票", "student");
		this.ticketText = this.ticketText.replaceAll("学生", "student");

		this.ticketText = this.ticketText.replaceAll("团体票价", "group");
		this.ticketText = this.ticketText.replaceAll("团体票", "group");
		this.ticketText = this.ticketText.replaceAll("团体", "group");

		this.ticketText = this.ticketText.replaceAll("捷克克朗", "czk");
		this.ticketText = this.ticketText.replaceAll("瑞典克朗", "sek");
		this.ticketText = this.ticketText.replaceAll("挪威克朗", "nok");
		this.ticketText = this.ticketText.replaceAll("丹麦克朗", "dkk");

	}

	/**
	 * 对币种简称和币种特殊符号进行统一. 货币的简称有多义词的情况, 例如"克朗",可能指"挪威克朗", "捷克克朗"等.
	 * 注意:这部分的统一操作包含两部分: 1)对于确定的币种替换为英文简称; 2)对于多义币种, 根据国家进行相应地英文简称替换.
	 */
	private void clean2() {
		// step1: 处理确定性币种.
		if (this.ticketText.contains("克朗")) {
			if (this.country.equals("挪威")) {
				this.ticketText = this.ticketText.replaceAll("克朗", "nok");
			} else if (this.country.equals("捷克")) {
				this.ticketText = this.ticketText.replaceAll("克朗", "czk");
			} else if (this.country.equals("瑞典")) {
				this.ticketText = this.ticketText.replaceAll("克朗", "sek");
			} else if (this.country.equals("丹麦")) {
				this.ticketText = this.ticketText.replaceAll("克朗", "dkk");
			} else {
				return;
			}
		}
		return;
		// step2: 处理多义币种.
	}

	/**
	 * 对数字格式进行统一.
	 */
	private void clean3() {

	}

	public static void main(String[] args) throws IOException {

	}
}
