package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class XAdultCHFParser implements Parsable {
	private static final Pattern ADULT_CHF_PAT = Pattern
			.compile("(adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:]?(chf(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})chf)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = ADULT_CHF_PAT.matcher(stt.getTicketText());
		return matcher.find();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> subGroups = RegexHelper.extractByGroup(
				stt.getTicketText(), ADULT_CHF_PAT.toString());
		List<String> priceList = RegexHelper.extractPriceList(subGroups.get(0));

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "chf");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

}
