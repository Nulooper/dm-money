package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class XAdultEURParser implements Parsable {
	private static final Pattern ADULT_EUR_PAT = Pattern
			.compile("(adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:,;]?(eur(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})eur)");

	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = ADULT_EUR_PAT.matcher(stt.getTicketText());
		return matcher.find();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> subGroups = RegexHelper.extractByGroup(
				stt.getTicketText(), ADULT_EUR_PAT.toString());
		List<String> priceList = RegexHelper.extractPriceList(subGroups.get(0));

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "eur");
	}

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	public static void main(String[] args) {
		String text = "adult:6eur持维也纳卡,残疾人,老人,军人,10人以上团队参观:4eur19岁以下:免费每月第一个周日:免费";

		List<String> groups = RegexHelper.extractByGroup(text,
				ADULT_EUR_PAT.toString());
		System.out.println(groups.get(0));
	}

}
