package com.mioji.parse;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mioji.ds.SpotRule;

public class XAdultHUFParser implements Parsable {
	private static final Pattern ADULT_HUF_PAT = Pattern
			.compile("(adult|全票|普通票|普通门票|每人|常规门票|通票|门票)[:]?(huf(\\d{1,}|\\d{1,}\\.\\d{1,})|(\\d{1,}|\\d{1,}\\.\\d{1,})huf)");
	@Override
	public boolean check(SpotTicketText stt) {
		Matcher matcher = ADULT_HUF_PAT.matcher(stt.getTicketText());
		return matcher.find();
	}

	@Override
	public String doParse(SpotTicketText stt) {
		List<String> subGroups = RegexHelper.extractByGroup(
				stt.getTicketText(), ADULT_HUF_PAT.toString());
		List<String> priceList = RegexHelper.extractPriceList(subGroups.get(0));

		return SpotRule.getOnlyOneTicket(stt.getId(), priceList.get(0), "huf");
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
